<?php

/**
 * Load different config files based on environment
 */

define( 'CONFIG_PATH', dirname(__FILE__) ); 											// cache it for multiple use
define( 'WP_LOCAL_SERVER', file_exists( CONFIG_PATH . '/wp-local-config.php' ) ); 		// store local database settings
define( 'WP_STAGING_SERVER', file_exists( CONFIG_PATH . '/wp-staging-config.php' ) ); 	// store staging database settings

if ( WP_LOCAL_SERVER ) {

	require CONFIG_PATH . '/wp-local-config.php';

} elseif ( file_exists( CONFIG_PATH . '/wp-staging-config.php' ) ) {

	require CONFIG_PATH . '/wp-staging-config.php';

} else {

	require CONFIG_PATH . '/wp-production-config.php';

}

/* Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/* The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 * Change these to different unique phrases!
 * https://api.wordpress.org/secret-key/1.1/salt/
 * @since 2.6.0
 */
 define('AUTH_KEY',         'o$?ER/+NZZa@|FgaU!avvJ89|n;M*i!g_ y,- ?P&MwYg=cm<[^OV|3R-vmNb->R');
 define('SECURE_AUTH_KEY',  ',4lgq!xn!VJ-$OnI-UZd9LNUbbXvztkYFQUiwcx5E5^:3c1Kw)#lLau|A+T/r:rm');
 define('LOGGED_IN_KEY',    'fI`4G$w#!$-cg[2M,c-`{&l0>w&PMiOZNDb%aMQAo}kLVO.;]=ydv23Qq&W-:6c|');
 define('NONCE_KEY',        '](;|4V9^Thg4aM+LAjd;~>+64c%:-^<kFzTgN~HA:F>ECN&^#:/}Mg>I+V#MfJ,<');
 define('AUTH_SALT',        'qfQt:J]hD.-l1:/FP+FzC1&.c 3h@FAVx-myd#q,`#djb2yQgI]fRXgweI,Y$jUh');
 define('SECURE_AUTH_SALT', ' ru&-1X~Yh]&O{yEhB=D/FmF*48NXxCs,G8huP{Z<[TE,p$F/?.yNWcJ^U2/`UC(');
 define('LOGGED_IN_SALT',   'E2r=NrM+ApnUk-j1W|v?m&-@5d]v$l_zo4LbTl8&$A3kgR/,-2QmA:sP{=t.R-nd');
 define('NONCE_SALT',       '8]mtr;Fcw-`j_+,3oOs(;!L5TvA?RofzhfVYPNE(y01DYN|H70IYc9lZM|OGWhhR');

/**
 * WordPress Database Table prefix.
 */
$table_prefix  = 'wpill_';

/**
 * WordPress Extras.
 */
define( 'AUTOSAVE_INTERVAL',    	3600 );   		// autosave 1x per hour
define( 'WP_POST_REVISIONS',    	false );  		// no revisions
define( 'DISABLE_WP_CRON',      	false );		// set WP cronjob
define( 'EMPTY_TRASH_DAYS',     	7 );      		// one week
define( 'DISALLOW_FILE_EDIT', 		true);			// disallow file edit in back-end

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
