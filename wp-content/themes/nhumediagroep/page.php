<?php get_header(); ?>

<main class="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part('parts/page/header'); ?>

		<?php ill_load_blocks(); ?>

	<?php endwhile; ?>

</main>

<?php get_footer(); ?>
