<?php 

$cases_title    = get_field('case_overview_title', 'options');
$cases_text     = get_field('case_overview_text', 'options');

get_header(); ?>

<main class="main">

    <div class="page-header">
        <div class="lg:grid lg:grid-cols-12 container">
            <div class="lg:col-start-3 lg:col-end-11">
                
                <?php if ( $cases_title ) : ?>
                <h1 class="h1">
                    <?php echo $cases_title; ?>
                </h1>
                <?php endif; ?>

                <?php echo $cases_text; ?>

            </div>
        </div>
    </div>

    <div class="cases__grid v cases__grid--bg">
        <div class="grid md:grid-cols-12 lg:grid-rows-2 container">

            <?php $counter = 0; while ( have_posts() ) : the_post(); $counter++; ?>

                <?php if ( $counter === 1 ) : ?>
                    <article data-aos="fade-up" class="cases__item cases__item--lg aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 md:aspect-ratio--21/9 lg:aspect-ratio--5/4 col-span-full lg:col-span-7 lg:row-span-2">
                <?php elseif ( $counter === 2 ) : ?>
                    <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 lg:aspect-ratio--auto col-span-full md:col-span-6 lg:col-span-5">
                <?php elseif ( $counter === 3 ) : ?>
                    <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 lg:aspect-ratio--auto col-span-full md:col-span-6 lg:col-span-5">
                <?php else : ?>
                    <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 col-span-full md:col-span-6">
                <?php endif; ?>
                    <a href="<?php the_permalink(); ?>" class="cases__inner flex flex-row items-end aspect-ratio__inner">

                        <?php the_post_thumbnail('large'); ?>

                        <div class="cases__content<?php if ( $counter === 2 ) echo ' cases__content--primary-lightest'; ?>">

                            <h3 class="cases__heading h5">
                                <?php the_title(); ?>
                            </h3>

                            <?php if ( get_field('case_overview_title') ) : ?>
                            <p class="h3">
                                <?php the_field('case_overview_title'); ?>
                            </p>
                            <?php endif; ?>

                        </div>

                    </a>
                </article>

                <?php if ( $counter === 3 ) : ?>

                    </div>

                </div>

                <div class="grid md:grid-cols-12 container">

                <?php endif; ?>

            <?php endwhile; ?>

        </div>

    <?php get_template_part('parts/global/pagination'); ?> 

</main>

<?php get_footer(); ?>