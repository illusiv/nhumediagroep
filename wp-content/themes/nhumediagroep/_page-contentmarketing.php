<?php  /* template name: Contentmarketing */ get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">

    <?php ill_get_template_part('parts/global/page-header', 				
        [
        'title' => 'Contentmarketing',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit massa et urna mollis, non fermentum velit semper',
        'image' => true,
        ]
    ); ?>

    <?php get_template_part('parts/global/timeline'); ?>

    <?php ill_get_template_part('parts/global/cta',
        [
        'double' => false
        ]
    ); ?>   

    <div class="block">
        <?php ill_get_template_part('parts/global/section-header', 				
            [
            'title' => 'Vertel het met formats',
            'text' => 'Korte intro over de kracht / het waarom van formats. Consectetur adipiscing elit. Integer condimentum auctor odio id iaculis:',
            ]
        ); ?>  
        
        <?php for ( $i = 0; $i < 2; $i++ ) : ?>
            <?php ill_get_template_part('parts/global/text-block',
                [
                    'tags' => false,
                    'button' => false,
                    'link' => false,
                ]
            ); ?> 
        <?php endfor; ?>
    </div>

    <?php ill_get_template_part('parts/global/cta',
        [
			'double' => false
			]
		); ?>

</main>

<?php endwhile; ?>

<?php get_footer() ?>
