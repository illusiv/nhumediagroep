
<?php /* template name: Contentcreatie */ get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">
    <?php ill_get_template_part('parts/global/page-header', 				
        [
        'title' => 'Contentcreatie',
        'text' => 'NHU Mediagroep vertaalt uw communcatie doelstellingen naar pakkende mediauitingen. Van banner tot blogpost. Van video tot advertentie. Voor publicatie op onze- of uw eigen kanalen.',
        'image' => true,
        ]
    ); ?>

    <div class="block">
        <div class="lg:grid lg:grid-cols-12 container">
            <div class="copy lg:col-start-2 lg:col-end-12">
                <h2>Waarom kiezen voor NHU Mediagroep?</h2>
                <p>Phasellus ut facilisis libero, at condimentum sem. Ut mauris ligula, condimentum lobortis scelerisque et, semper quis felis. Etiam eleifend eros ac mollis consequat. Nullam placerat tincidunt consequat.</p>
                <figure>
                    <blockquote>
                        <p>Omdat we uw taal spreken en die van uw doelgroep</p>
                    </blockquote>
                    <figcaption>Herman Grendelman</figcaption>
                </figure>
                <ul>
                    <li>Geen copywriter maar bouwredacteur</li>
                    <li>Geen reclamebureau maar bouwbureau</li>
                    <li>Punchline plaatsen</li>
                    <li>Punchline plaatsen</li>
                </ul>
            </div>
        </div>
    </div>

    <?php get_template_part('parts/global/usps'); ?>

    <?php get_template_part('parts/global/testimonials'); ?>

    <?php ill_get_template_part('parts/global/text-block',
        [
            'tags' => false,
            'button' => false,
            'link' => false
        ]
    ); ?> 

    <div class="block">
        <?php ill_get_template_part('parts/global/section-header', 				
            [
            'title' => 'Cases uit de praktijk',
            'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac orci vestibulum, efficitur massa non, semper lacus. Nulla ultrices ornare nisl id auctor. Proin sed pharetra mauris',
            ]
        ); ?>   

        <?php ill_get_template_part('parts/cases-grid', 				
            [
            'bg' => false,
            ]
        ); ?>
    </div>

    <?php get_template_part('parts/global/contact'); ?>

</main>

<?php endwhile; ?>

<?php get_footer(); ?>


