<?php

/**
 * Assets
 *
 */

class JsonManifest {

	private $manifest;

	public function __construct( $manifest_path ) {

		if ( file_exists( $manifest_path ) ) {
			$this->manifest = json_decode( file_get_contents( $manifest_path ), true );
		} else {
			$this->manifest = [ ];
		}

	}

	public function get() {
		return $this->manifest;
	}

	public function getPath( $key = '', $default = null ) {

		$collection = $this->manifest;

		if ( is_null( $key ) ) {
			return $collection;
		}

		if ( isset( $collection[ $key ] ) ) {
			return $collection[ $key ];
		}

		foreach ( explode( '.', $key ) as $segment ) {
			if ( ! isset( $collection[ $segment ] ) ) {
				return $default;
			} else {
				$collection = $collection[ $segment ];
			}
		}

		return $collection;

	}

}


/**
 * Get paths for assets
 */

function ill_asset_path( $filename ) {

	$dist_path = get_template_directory_uri() . '/dist';

	static $manifest;

	$filename = '/' . $filename;

	if ( empty( $manifest ) ) {
		$manifest_path = get_template_directory() . '/dist/' . 'mix-manifest.json';
		$manifest      = new JsonManifest( $manifest_path );
	}

	if ( array_key_exists( $filename, $manifest->get() ) ) {
		return $dist_path . $manifest->get()[ $filename ];
	} else {
		return $dist_path . $filename;
	}

}


/**
 * Enqueue assets
 */

function ill_enqueue_assets() {

	// Enqueue styles.
	wp_enqueue_style( 'main', ill_asset_path( 'styles/main.css' ), false, null );

	// Enqueue scripts.
	wp_enqueue_script( 'main', ill_asset_path( 'scripts/main.js' ), array( 'jquery' ), null, true );

}

add_action( 'wp_enqueue_scripts', 'ill_enqueue_assets' );
