<?php

/**
 * Theme shortcodes
 *
 */

// Add Shortcode Widget Support
add_filter('widget_text', 'do_shortcode');

// Register shortcodes
add_shortcode('button', 'ill_sc_button');
add_shortcode('toggle', 'ill_sc_toggle');

// Shortcode Button
function ill_sc_button($atts, $content = null) {
	extract(shortcode_atts(array(
		'link' 		=> '#',
		'target' 	=> '_self'
	), $atts));
	return '<a class="button button--primary button--arrow" href="' . $link . '" target="' . $target . '">' . do_shortcode($content) . '</a>';
}

// Shortcode Toggle
function ill_sc_toggle($atts, $content = null) {
	extract(shortcode_atts(array('title' => '#'), $atts));
	return ' <h5 class="toggle"><span>' . $title . '</span></h5>
	<div class="toggle-box">
		' . do_shortcode($content) . '
	</div>';
}
