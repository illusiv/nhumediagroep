<?php

/**
 * Custom post types & taxonomies
 *
 */

add_action('init', 'ill_register_post_types');
//add_action('init', 'ill_register_taxonomies');

/**
 * Register custom post types.
 */
function ill_register_post_types()
{
	register_post_type(
		'case',
		array(
			'labels' => array(
				'name' 					=> 'Cases',
				'singular_name' 		=> 'Case',
				'add_new' 				=> __( 'Nieuwe cases toevoegen' ),
				'add_new_item' 			=> __( 'Nieuwe case toevoegen' ),
				'edit' 					=> __( 'Bewerken' ),
				'edit_item' 			=> __( 'Bewerk item' ),
				'new_item' 				=> __( 'Nieuw item' ),
				'view' 					=> __( 'Bekijken' ),
				'view_item' 			=> __( 'Bekijk item' ),
				'search_items' 			=> __( 'Zoek naar item' ),
				'not_found' 			=> __( 'Geen items gevonden' ),
				'not_found_in_trash' 	=> __( 'Geen items gevonden in de prullenbak' )
			),
			'public' 					=> true,
			'has_archive'				=> 'cases',
			'hierarchical' 				=> false,
			'menu_icon'					=> 'dashicons-portfolio',
			'capability_type' 			=> 'page', 'post',
			'rewrite' 					=> array(
				'slug' 					=> 'case',
				'with_front' 			=> false
			),
			'show_ui' 					=> true,
			'supports' 					=> array(
				'title',
				'editor',
				'thumbnail',
				'page-attributes'
			)
		)
	);
}

/**
 * Register custom taxonomies.
 */
function ill_register_taxonomies() {
	register_taxonomy('posttype-category', 'posttype', array('hierarchical' => true, 'label' => __('Categorie&euml;n')));
}
