<?php

/**
 * Theme defaults
 *
 */

add_action('init', 'ill_register_menus');
add_action('init', 'ill_init_thumbnails');

/**
 * Register navigation menus. Hooked to init.
 */
function ill_register_menus() {

	register_nav_menus(
		array(
			'primary-menu' 		=> 'Primary',
			'secondary-menu' 	=> 'Secondary',
			'footer-menu' 		=> 'Footer'
		)
	);

}


/**
 * Setup theme image sizes. Hooked to init.
 */
function ill_init_thumbnails() {

	// Add thumbnails support
	add_theme_support('post-thumbnails');

	// Add appropriate image sizes
	add_image_size('278x360', 278, 360, true);
	add_image_size('480x480', 480, 480, true);
	add_image_size('556x860', 556, 860, true);
	//add_image_size('blog', 180, 150, true);

	// Force default images hard crop
	add_image_size('medium', get_option( 'medium_size_w' ), get_option( 'medium_size_h' ), true);

}
