<?php

/**
 * Custom branding
 *
 */

// Register actions and filters
add_action( 'admin_head', 'ill_favicon' );
add_action( 'wp_head', 'ill_favicon' );
add_filter( 'admin_footer_text', 'ill_admin_footer_text' );
add_action( 'admin_menu', 'ill_remove_menus' );


/**
 * Add a favicon for front and backend
 */

function ill_favicon() {
 	echo '<link rel="apple-touch-icon" sizes="180x180" href="'. ill_asset_path('images/favicon.png') .'">';
 	echo '<link rel="icon" type="image/png" href="'. ill_asset_path('images/favicon.png') .'">';
}


/**
 * Filter to replace the default Wordpress admin-footer.
 */
function ill_admin_footer_text() {

	$footer = 'WordPress Website door <a href="https://www.illusiv.nl" target="_blank">illusiv.nl</a>, '.
	'Freelance Webdesigner &amp; WordPress Developer';

	return $footer;

}


/**
 * Remove WP generator tag from <head>
 */

remove_action('wp_head', 'wp_generator');


/**
 * Removes every menu for all users. To remove only certain menu items include only those you want to hide within the function.
 * To remove menus for only certain users you may want to utilize current_user_can().
 *
 */

function ill_remove_menus() {

	/*
	remove_menu_page( 'index.php' );                  //Dashboard
	remove_menu_page( 'edit.php' );                   //Posts
	remove_menu_page( 'upload.php' );                 //Media
	remove_menu_page( 'edit.php?post_type=page' );    //Pages
	remove_menu_page( 'edit-comments.php' );          //Comments
	remove_menu_page( 'themes.php' );                 //Appearance
	remove_menu_page( 'plugins.php' );                //Plugins
	remove_menu_page( 'users.php' );                  //Users
	remove_menu_page( 'tools.php' );                  //Tools
	remove_menu_page( 'options-general.php' );        //Settings
	*/

	remove_menu_page( 'edit-comments.php' );

}
