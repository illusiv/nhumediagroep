<?php

/**
 * Move Yoast's filthy metabox to the bottom where it belongs.
 *
 * @return string Priority of the metabox.
 */
function ill_move_yoast_metabox() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'ill_move_yoast_metabox' );


/**
 * Page blocks loading function
 *
 */
function ill_load_blocks( $post_id = null ) {

	if ( ! $post_id ) {
		$post_id = get_the_ID();
	}

	if ( ! have_rows( 'blocks', $post_id ) ) {
		return;
	}

	while ( have_rows( 'blocks', $post_id ) ) : the_row();

		$layout = get_row_layout();

		if ( false !== strpos( $layout, '_' ) ) {
			$layout = str_replace( '_', '-', $layout );
		}

		get_template_part( 'parts/blocks/block', $layout );

	endwhile;

}


/**
 * Remove Gutenberg Block Library CSS from loading on the frontend
 *
 */
function ill_remove_wp_block_library_css(){
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
}
add_action( 'wp_enqueue_scripts', 'ill_remove_wp_block_library_css', 100 );


/**
 * Disable the emoji's
 */
function ill_disable_emojis() {

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

}
add_action( 'init', 'ill_disable_emojis' );


/**
 * Filter out the tinymce emoji plugin.
 */
function ill_disable_emojis_tinymce( $plugins ) {

	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}

}


/**
 * Disable default WP gallery styles
 *
 */

add_filter( 'use_default_gallery_style', '__return_false' );


/**
 * Include a template and pass it a model or arguments.
 *
 * Effectively the same as WordPress' get_template_part only allows for passing of arguments & models.
 *
 * Note: When an array is passed it will extract the arguments and make them available for the included template.
 *  When anything else is passed it will just pass the $model.
 *  The $model variable is always available to the included template.
 *
 * @param string|array $template_names The name of the template that should be fetched (or multiple options, uses first find)
 * @param array|mixed $model A model or an array of arguments
 * @param boolean $echo Should output the results
 *
 * @return bool|string file has been included or result if echo is false
 *
 * @see get_template_part()
 */

if ( ! function_exists( 'ill_get_template_part' ) ) {

	function ill_get_template_part( $template_names, $model = [], $echo = true ) {

		// Append .php as locate template doesn't support that
		$template_names = (array) $template_names;
		foreach ( $template_names as $index => $template_name) {
			$template_names[$index] = $template_name . '.php';
		}

		// Try to find the template
		$path = locate_template( $template_names, false );

		if ( ! empty( $path ) ) {

			if ( ! empty( $model ) && is_array( $model ) ) {
				// Make any additional vars available for the script
				extract( $model );
			}

			// Unset the variables to prevent accidental access
			unset( $template_names );

			// Include the file (buffered if required)
			if ( ! $echo ) {
				ob_start();
			}

			include $path;

			if ( $echo ) {
				return true;
			} else {
				$result = ob_get_contents();
				ob_end_clean();

				return $result;
			}
		} else {
			return false;
		}

	}

}