<?php

/**
 * Advanced Custom Fields (ACF)
 */


// ACF JSON SAVE
function ill_acf_json_save_point( $path ) {
	// update path
	$path = get_stylesheet_directory() . '/custom-fields';
	// return
	return $path;
}
add_filter( 'acf/settings/save_json', 'ill_acf_json_save_point' );


// ACF JSON LOAD
function ill_acf_json_load_point( $paths ) {
	// remove original path (optional)
	unset( $paths[0] );
	// append path
	$paths[] = get_stylesheet_directory() . '/custom-fields';
	// return
	return $paths;
}
add_filter( 'acf/settings/load_json', 'ill_acf_json_load_point' );


// ACF options pages
if ( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Thema opties',
		'menu_title'	=> 'Thema opties',
		'menu_slug' 	=> 'theme-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	// Post type: case
	acf_add_options_page(array(
		'page_title' 	=> 'Cases instellingen',
		'menu_title'	=> 'Instellingen',
		'parent_slug'	=> 'edit.php?post_type=case',
		'menu_slug' 	=> "case-options",
		'capability'	=> 'edit_posts'
	));

}
