<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @author      Tommy Kolkman <tommy@steez.nl>
 * @copyright   Copyright (c) 2020 Steez (https://steez.nl)
 * @license     http://opensource.org/licenses/gpl-3.0.en.php General Public License (GPL 3.0)
 *
 */

/**
 * Noindex check
 *
 * Find out what environment we are on and update the noindex (blog_public)
 * option accordingly.
 *
 * @return void;
 */

add_action('init', 'ill_core_noindex');

function ill_core_noindex ()
{
	/** Stop if setting is cached */
	if (!empty(get_transient('ill_index'))) {
		return;
	}
	
	/** Check what environment we are running on */
	if (!defined('WP_LOCAL_SERVER') && !defined('WP_STAGING_SERVER')) {
		/** No environment constants defined, check another way */
		if (false !== stripos(site_url(), 'steez') || false !== stripos(site_url(), 'localhost') || false !== stripos(site_url(), '.test')) {
			$index = false;
		} /** Don't index */
		else {
			$index = true;
		}
		/** Index */
	} else {
		/** We have environment constants to work with */
		if (false == WP_LOCAL_SERVER && false == WP_STAGING_SERVER) {
			$index = true;
		} /** Not on local or staging, so index */
		else {
			$index = false;
		}
	}
	
	/** Allow overwrite through filters (for intranets i.e.) */
	$index = apply_filters('ill_override_noindex', $index);
	
	/** Update option and cache setting */
	update_option('blog_public', $index);
	set_transient('ill_index', $index, DAY_IN_SECONDS);
}
