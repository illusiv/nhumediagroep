<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">

	<header class="page-header<?php if ( has_post_thumbnail() ) echo ' page-header--with-image'; ?>">

		<div class="lg:grid lg:grid-cols-12 container">

			<div class="lg:col-start-3 lg:col-end-11">

				<h1 class="h1">
					<?php the_title(); ?>
				</h1>

				<?php if ( get_field('case_overview_title') ) : ?>
					<p class="page-header__subtitle" style="color: #12B3E5;"><?php the_field('case_overview_title'); ?></p>
				<?php else : ?>
					<p class="page-header__subtitle"><?php printf( __('Geplaatst op %s', 'nhumediagroep'), get_the_time('d F Y') ); ?></p>
				<?php endif; ?>

			</div>

		</div>

		<?php if ( has_post_thumbnail() ) : ?>
			<div class="page-header__media aspect-ratio aspect-ratio--21/9 lg:grid lg:grid-cols-12">
				<div class="page-header__media-inner aspect-ratio__inner lg:col-start-2 lg:col-end-12">
					<?php the_post_thumbnail('large'); ?>
				</div>
			</div>                
		<?php endif; ?>

	</header>

	<div class="container">
		<div class="block">
			<div class="lg:grid lg:grid-cols-12 container">

				<article class="copy lg:col-start-2 lg:col-end-12">
					<?php the_content(); ?>
				</article>

			</div>
		</div>
	</div>

</main>

<?php endwhile; ?>

<?php get_footer(); ?>
