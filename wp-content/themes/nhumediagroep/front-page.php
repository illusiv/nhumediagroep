<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">

    <?php get_template_part('parts/home/hero'); ?>

    <?php get_template_part('parts/home/quick-links'); ?>

    <?php get_template_part('parts/home/platforms'); ?>

    <?php get_template_part('parts/home/cases'); ?>

    <?php get_template_part('parts/home/testimonials'); ?>

</main>

<?php endwhile; ?>

<?php get_footer(); ?>
