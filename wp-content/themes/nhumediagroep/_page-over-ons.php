
<?php /* template name: Over ons */ get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">
    <?php ill_get_template_part('parts/global/page-header', 				
        [
        'title' => 'Wij zijn NHU Mediagroep',
        'text' => 'NHU Mediagroep is 20 jaar ervaring in bouwmedia. NHU denkt, praat en beweegt de bouw en helpt... hier een punchline.',
        'image' => false,
        ]
    ); ?>

    <?php for ( $i = 0; $i < 2; $i++ ) : ?>
        <?php ill_get_template_part('parts/global/text-block',
            [
                'tags' => false,
                'button' => false,
                'link' => false
            ]
        ); ?> 
    <?php endfor; ?>
    
    <?php get_template_part('parts/global/team'); ?>

    <?php get_template_part('parts/global/logos'); ?>

    <?php ill_get_template_part('parts/global/cta',
    [
        'double' => false
        ]
    ); ?>

</main>

<?php endwhile; ?>

<?php get_footer(); ?>
