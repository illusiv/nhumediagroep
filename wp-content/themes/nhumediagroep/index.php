<?php get_header(); ?>

<div class="page-header">
    <div class="lg:grid lg:grid-cols-12 container">
        <div class="lg:col-start-3 lg:col-end-11">
            <h1 class="h1">
                <?php echo get_the_title( get_option( 'page_for_posts' ) ); ?>
            </h1>
        </div>
    </div>
</div>

<div class="container">

    <?php if ( ! have_posts() ) : ?>
    <p><?php _e('Helaas, er zijn geen resultaten gevonden.','nhumediagroep'); ?></p>
    <?php endif; ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <div data-aos="fade-up" class="block text-block">
            <div class="md:grid md:grid-cols-12 xl:items-center container">

                <?php if ( has_post_thumbnail() ) : ?>
                <div class="aspect-ratio aspect-ratio--16/9 md:aspect-ratio--square md:col-span-6 lg:col-start-1 lg:col-end-5 xl:col-start-2 xl:col-end-6">
                    <a href="<?php the_permalink(); ?>" class="text-block__image aspect-ratio__inner">
                        <?php the_post_thumbnail('medium'); ?>
                    </a>
                </div>
                <?php endif; ?>

                <div class="md:col-span-6 lg:col-start-5 lg:col-end xl:col-start-6 xl:col-end-12">
                    <div class="text-block__content">

                        <h3 class="heading-underline h2">
                            <?php the_title(); ?>
                        </h3>

                        <p><?php printf( __('Geplaatst op %s', 'nhumediagroep'), get_the_time('d F Y') ); ?></p>

                        <?php the_excerpt(); ?>

                        <a href="<?php the_permalink(); ?>" class="button button--primary button--arrow">
                            <?php _e('Lees verder','nhumediagroep'); ?>
                        </a>

                    </div>
                </div>

            </div>
        </div>

    <?php endwhile; ?>

    <?php get_template_part('parts/global/pagination'); ?> 

</div>

<?php get_footer(); ?>
