<div class="cases__grid v <?= $bg ? 'cases__grid--bg' : '' ?>">
    <div class="grid md:grid-cols-12 lg:grid-rows-2 container">

        <article data-aos="fade-up" class="cases__item cases__item--lg aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 md:aspect-ratio--21/9 lg:aspect-ratio--5/4 col-span-full lg:col-span-7 lg:row-span-2">
            <a href="#" class="cases__inner flex flex-row items-end aspect-ratio__inner">
                <img src="<?php echo ill_asset_path( 'images/case-1.jpg' ); ?>" alt="case image">
                <div class="cases__content">
                    <h3 class="cases__heading h5">Contentmarketing Alfa Accountants</h3>
                    <p class="h3">Onze content is relevant, dat merken we aan de reacties</p>
                </div>
            </a>
        </article>

        <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 lg:aspect-ratio--auto col-span-full md:col-span-6 lg:col-span-5">
            <a href="#" class="cases__inner flex flex-row items-end aspect-ratio__inner">
                <img src="<?php echo ill_asset_path( "images/case-2.jpg" ); ?>" alt="case image">
                <div class="cases__content <?= $bg ? 'cases__content--primary-lightest' : '' ?>">
                    <h3 class="cases__heading h5">Project in Beeld Kingspan</h3>
                    <p class="h3">Onze doelgroep inspireren op LinkedIn</p>
                </div>
            </a>
        </article>

        <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 lg:aspect-ratio--auto col-span-full md:col-span-6 lg:col-span-5">
            <a href="#" class="cases__inner flex flex-row items-end aspect-ratio__inner">
                <img src="<?php echo ill_asset_path( "images/case-3.jpg" ); ?>" alt="case image">
                <div class="cases__content">
                    <h3 class="cases__heading h5">Project in Beeld Kingspan</h3>
                    <p class="h3">Onze doelgroep inspireren op LinkedIn</p>
                </div>
            </a>
        </article>

    </div>
    
</div>
    <?php if ( is_post_type_archive('case') ): ?>
        
        <div class="grid md:grid-cols-12 container">
            
            <?php for ( $i = 0; $i < 6; $i++ ) : ?>
                <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 col-span-full md:col-span-6">
                    <a href="#" class="cases__inner flex flex-row items-end aspect-ratio__inner">
                        <img src="<?php echo ill_asset_path( "images/case-0.jpg" ); ?>" alt="case image">
                        <div class="cases__content">
                            <h3 class="cases__heading h5">Project in Beeld Kingspan</h3>
                            <p class="h3">Onze doelgroep inspireren op LinkedIn</p>
                        </div>
                    </a>
                </article>
            <?php endfor; ?>
            
        </div>
        
     <?php endif; ?>
     