<?php

$page_title = get_field('page_title');
$page_text  = get_field('page_text');
$page_image = wp_get_attachment_image( get_field('page_image'), 'full' );

?>
<div class="page-header<?php if ( $page_image ) echo ' page-header--with-image'; ?>">

    <div class="lg:grid lg:grid-cols-12 container">

        <div class="lg:col-start-3 lg:col-end-11">
            
            <?php if ( $page_title ) : ?>
            <h1 class="h1"><?php echo $page_title; ?></h1>
            <?php endif; ?>

            <?php echo $page_text; ?>

        </div>

    </div>

    <?php if ( $page_image ): ?>
        <div class="page-header__media aspect-ratio aspect-ratio--21/9 lg:grid lg:grid-cols-12">
            <div class="page-header__media-inner aspect-ratio__inner lg:col-start-2 lg:col-end-12">
                <?php echo $page_image; ?>
            </div>
        </div>                
    <?php endif; ?>

</div>