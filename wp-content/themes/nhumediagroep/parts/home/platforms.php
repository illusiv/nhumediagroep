<?php

$titles_title           = get_field('titles_title');
$titles_text            = get_field('titles_text');
$titles_platforms       = 'titles_platforms';

?>
<section data-aos="fade-up" class="block">

    <div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

        <?php if ( $titles_title ) : ?>
        <div class="lg:col-start-4 lg:col-end-10">

            <h2 class="heading-underline heading-underline--center h2">
                <?php echo $titles_title; ?>
            </h2>

            <?php echo $titles_text; ?>

        </div>
        <?php endif; ?>

    </div>

    <?php if ( get_field( $titles_platforms ) ) : ?>
        <div class="platforms-wrapper">
            <div class="platforms flex sm:grid sm:grid-cols-12 sm:container">

                <?php while ( has_sub_field( $titles_platforms ) ) : ?>

                    <?php
                        $title  = get_sub_field('title');
                        $link   = get_sub_field('link');
                        $logo   = wp_get_attachment_image_url( get_sub_field('logo'), 'full' );
                        $image  = wp_get_attachment_image_url( get_sub_field('image'), '278x360' );
                    ?>

                    <article class="platforms__item aspect-ratio aspect-ratio--7/9 sm:col-span-6 lg:col-span-3">
                        <div class="platforms__inner aspect-ratio__inner">
                            <a href="<?php echo $link; ?>">

                                <div class="platforms__content flex flex-col">

                                    <?php if ( $title ) : ?>
                                    <h3 class="platforms__heading">
                                        <?php echo $title; ?>
                                    </h3>
                                    <?php endif; ?>

                                    <?php if ( $logo ) : ?>
                                    <img class="platforms__logo lazyload" data-src="<?php echo $logo; ?>" alt="<?php echo $title; ?> logo">
                                    <?php endif; ?>

                                    <i class="fal fa-lg fa-long-arrow-right"></i>

                                </div>

                                <?php if ( $image ) : ?>
                                <img class="platforms__media lazyload" width="278" height="360" data-src="<?php echo $image; ?>" alt="<?php echo $title; ?> image">
                                <?php endif; ?>

                            </a>
                        </div>
                    </article>

                <?php endwhile; ?>

            </div>
        </div>
    <?php endif; ?>

</section>