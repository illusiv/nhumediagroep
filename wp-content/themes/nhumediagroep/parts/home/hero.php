<?php

$panorama_title     = get_field('panorama_title');
$panorama_text      = get_field('panorama_text');
$panorama_button    = get_field('panorama_button');
$panorama_image     = wp_get_attachment_image( get_field('panorama_image'), 'large' );

?>
<section class="hero">
    <div class="hero__inner lg:grid lg:grid-cols-12 lg:container">

        <?php if ( $panorama_image ) : ?>
        <div class="hero__media aspect-ratio aspect-ratio--3/2 lg:col-start-6 lg:col-end-12">
            <div class="aspect-ratio__inner">
                <?php echo $panorama_image; ?>
            </div>
        </div>
        <?php endif; ?>

        <div class="hero__content lg:flex lg:flex-col lg:col-start-1 lg:col-end-6 lg:row-start-1">

            <?php if ( $panorama_title ) : ?>
            <h1><?php echo $panorama_title; ?></h1>
            <?php endif; ?>

            <?php echo $panorama_text; ?>

            <?php if ( $panorama_button ) : ?>
            <a href="<?php echo $panorama_button['url']; ?>" class="button button--primary button--arrow" target="<?php echo $panorama_button['target']; ?>">
                <?php echo $panorama_button['title']; ?>
            </a>
            <?php endif; ?>

        </div>

    </div>
</section>