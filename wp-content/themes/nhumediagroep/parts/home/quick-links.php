<?php

$quick_links_title      = get_field('quick_links_title');
$quick_links_text       = get_field('quick_links_text');
$quick_links_items      = 'quick_links_items';

?>
<section data-aos="fade-up" class="block">

    <div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

        <?php if ( $quick_links_title ) : ?>
        <div class="lg:col-start-4 lg:col-end-10">

            <h2 class="heading-underline heading-underline--center h2">
                <?php echo $quick_links_title; ?>
            </h2>

            <?php echo $quick_links_text; ?>

        </div>
        <?php endif; ?>

    </div>

    <?php if ( get_field( $quick_links_items ) ) : ?>
        <div class="flex flex-wrap justify-center container">

            <?php while ( has_sub_field( $quick_links_items ) ) : ?>

                <div class="w-full md:w-1/2 lg:w-1/3">

                    <?php
                        $title		    = get_sub_field('title');
                        $link		    = get_sub_field('link');
                        $image_id		= get_sub_field('icon');
                        $image_url		= wp_get_attachment_image_url( $image_id, 'large' );
                        $image_alt      = get_post_meta( $image_id, '_wp_attachment_image_alt', TRUE);
                        $image_title    = get_the_title( $image_id );
                    ?>

                    <article class="card">
                        <a id="inline" href="<?php echo get_the_permalink($link); ?>" class="flex flex-col">

                            <?php if ( $image_id ) : ?>
                                <img class="lazyload card__icon" width="64" height="64" data-src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $image_title; ?>">
                            <?php endif; ?>

                            <?php if ( $title ) : ?>
                            <h3 class="card__heading h3">
                                <?php echo $title ?>
                            </h3>
                            <?php endif; ?>

                            <i class="fal fa-lg fa-long-arrow-right"></i>

                        </a>
                    </article>

                </div>

            <?php endwhile; ?>

        </div>
    <?php endif; ?>

</section>