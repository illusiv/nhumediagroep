<ul class="socials md:flex">
    <li>
        <a href="#">                    
            <i class="fab fa-sm fa-linkedin-in"></i>
        </a>
    </li>
    <li>
        <a href="#">                    
            <i class="fab fa-sm fa-facebook-f"></i>
        </a>
    </li>
    <li>
        <a href="#">                    
            <i class="fal fa-sm fa-envelope-open"></i>
        </a>
    </li>
</ul>