<div class="page-header <?php echo $image ? 'page-header--with-image' : ''; ?>">

    <div class="lg:grid lg:grid-cols-12 container">

        <div class="lg:col-start-3 lg:col-end-11">
            <h1 class="h1"><?= $title ?></h1>

            <?php if ( !empty($text) ): ?>
                <p><?= $text ?></p>
            <?php endif; ?>

        </div>
        
    </div>

    <?php if ( !empty($image) ): ?>
        <div class="page-header__media aspect-ratio aspect-ratio--21/9 lg:grid lg:grid-cols-12">
            <div class="page-header__media-inner aspect-ratio__inner lg:col-start-2 lg:col-end-12">
                <img src="<?php echo ill_asset_path( 'images/hero-image.jpg' ); ?>" alt="page-header image">
            </div>
        </div>                
    <?php endif; ?>

</div>