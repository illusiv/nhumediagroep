<div data-aos="fade-up" class="logos block">
    <?php ill_get_template_part('parts/global/section-header', 				
    [
        'title' => 'Ook zij werken samen met NHU Mediagroep',
    ]
    ); ?>
    <ul class="grid items-center justify-items-center container">
        <?php for ( $i = 0; $i < 5; $i++ ) : ?> 
            <li>
                <a href="https://google.nl" target="_blank" rel="noopener noreferrer">
                    <img src="<?php echo ill_asset_path( 'images/logo-' . $i . '.png' ); ?>" alt="Logo">
                </a>
            </li>
        <?php endfor; ?>
    </ul>
</div>