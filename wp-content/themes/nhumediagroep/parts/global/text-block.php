<!-- @Melvin: Add text-block--reversed to reverse a text-block -->
<div data-aos="fade-up" class="text-block block">
    <div class="md:grid md:grid-cols-12 xl:items-center container">
        <div class="aspect-ratio aspect-ratio--16/9 md:aspect-ratio--square md:col-span-6 lg:col-start-1 lg:col-end-5 xl:col-start-2 xl:col-end-6">
            <a href="#" class="text-block__image aspect-ratio__inner">
                <img src="<?php echo ill_asset_path( "images/case-1.jpg" ); ?>" alt="image">
            </a>
        </div>
        <div class="md:col-span-6 lg:col-start-5 lg:col-end xl:col-start-6 xl:col-end-12">
            <div class="text-block__content">
                <h3 class="heading-underline h2">Brand Content</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum auctor odio id iaculis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus ut facilisis libero, at condimentum sem.</p>
                
                <?php if ( !empty($tags) ): ?>
                <div class="tags">
                    <strong>Geschikt voor</strong>
                    <ul class="flex flex-wrap tags__list">
                        <?php for ( $j = 0; $j < 4; $j++ ) : ?>
                            <li class="tags__item">Tag item</li>
                        <?php endfor; ?>
                    </ul>
                </div>
                <?php endif; ?>

                <?php if ( !empty($button) || !empty($link) ): ?>
                    <ul class="inline-flex flex-wrap flex-col">
                        <?php if ( !empty($button) ): ?>
                        <li>
                            <a href="/adverteren/advertorials" class="button button--primary button--arrow">Meer informatie</a>
                        </li>
                        <?php endif; ?>

                        <?php if (!empty($link)): ?>
                        <li>
                            <a href="/" class="button button--link"><i class="fal fa-info-circle"></i><span>Aanleverspecificaties</span></a>
                        </li>
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>