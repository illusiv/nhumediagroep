<section class="block">
    <?php ill_get_template_part('parts/global/section-header', 				
        [
        'title' => 'Wat mogen we voor u maken?',
        'text' => 'Naast advertorials bieden wij dolor sit amet, consectetur adipiscing elit. Integer condimentum auctor odio id iaculis:',
        ]
    ); ?>

    <div class="flex flex-wrap justify-center container">

        <?php for ( $i = 0; $i < 7; $i++ ) : ?>
            <div class="w-full md:w-1/3 lg:w-1/4" data-fancybox data-src="#data" data-auto-focus="false">
                <?php ill_get_template_part('parts/global/card', 				
                    [
                    'title' => 'Banners',
                    'icon' => true
                    ]
                ); ?> 
            </div>
        <?php endfor; ?>

    </div>
</section>