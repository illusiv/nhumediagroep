<article class="card">
    <a id="inline" href="#data" class="flex flex-col">

        <?php if ( !empty($icon) ): ?>
            <img class="card__icon" src="<?php echo ill_asset_path( "images/usp-0.svg" ); ?>" alt="<?= $title ?>">
        <?php endif; ?>

        <h3 class="card__heading h3"><?= $title ?></h3>

        <?php if ( !empty($text) ): ?>
            <p><?= $text ?></p>
        <?php endif; ?> 
        
        <i class="fal fa-lg fa-long-arrow-right"></i>
    </a>
</article>