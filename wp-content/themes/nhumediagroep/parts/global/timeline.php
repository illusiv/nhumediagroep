<section data-aos="fade-up" class="timeline container">

        <h2 class="timeline__heading">Contentmarketing in 3 fundamentele stappen</h2>
        
        <div class="timeline__inner">
            <ul class="timeline__list">
                <?php for ( $i = 0; $i < 3; $i++ ) : ?>
                    <div class="timeline__item ">
                        <?php ill_get_template_part('parts/global/card', 				
                            [
                            'title' => 'Distributie & pm',
                            'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec dolor sit amet sem efficitur laoreet ac nec nisi. Pellentesque varius, tellus ac mattis volutpat, dui leo imperdiet purus.',
                            'icon' => $i
                            ]
                        ); ?> 
                    </div>
                <?php endfor; ?>
            </ul>
        </div>


</section>