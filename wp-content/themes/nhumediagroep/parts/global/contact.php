<div data-aos="fade-up" class="contact block">
    <div class="lg:grid lg:grid-cols-12 container">
        <div class="lg:col-start-2 lg:col-end-12 xl:col-start-3 xl:col-end-11">
            <aside class="contact__card">
                <div class="md:flex">
                    <img class="avatar" src="<?php echo ill_asset_path( "images/avatar.png" ); ?>" alt="Profile avatar">
                    <div class="contact__info">
                        <h2 class="h3">Meer weten adverteren?</h2>
                        <p>Neem vrijblijvend contact op met Hugo Arends:</p>
                        <ul class="flex flex-wrap flex-col justify-center md:flex-row md:justify-start">
                            <li>
                                <a href="#"><i class="fal fa-mobile"></i>0570-861007</a>
                            </li>
                            <li>
                                <a href="#"><i class="fal fa-envelope-open"></i>stuur een mail</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>