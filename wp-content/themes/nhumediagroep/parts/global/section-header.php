<div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

    <div class="lg:col-start-4 lg:col-end-10">
        <!-- Title variable -->
        <h2 class="heading-underline heading-underline--center h2"><?= $title ?></h2>
        <!-- Text variable - optional -->
        <?php if ( !empty($text) ): ?>
            <p><?= $text ?></p>
        <?php endif; ?>
    </div>
    
</div>