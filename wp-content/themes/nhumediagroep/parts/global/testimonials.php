<section data-aos="fade-up" class="block">

    <?php ill_get_template_part('parts/global/section-header', 				
        [
        'title' => 'Wat klanten zeggen...',
        ]
    ); ?>    

    <div class="lg:grid lg:grid-cols-12 container">
        <div class="lg:col-start-2 lg:col-end-12 xl:col-start-3 xl:col-end-11">

            <div class="testimonials swiper-container">
            
                <div class="swiper-wrapper">

                    <?php for ( $i = 0; $i < 3; $i++ ) : ?>
                        <div class="testimonials__item swiper-slide">
                            <div class="testimonials__media">
                                <div class="aspect-ratio aspect-ratio--square">
                                    <div class="aspect-ratio__inner">
                                        <img src="<?php echo ill_asset_path( "images/testimonial-{$i}.jpg" ); ?>" alt="testimonial media">
                                    </div>
                                </div>
                            </div>
                            <figure class="testimonials__content">
                                <blockquote>
                                    <p>Naast advertising op de juiste posities ons merk laden door het vertellen van ons eigen merkverhaal. Vivamus nec nisi fringilla.</p>
                                </blockquote>
                                <figcaption>Arap-John Tigchelaar<cite>Directeur Transferro</cite></figcaption>
                            </figure>
                        </div>
                    <?php endfor; ?>

                </div>

                <!-- If we need navigation buttons -->
                <div class="testimonials__buttons">
                    <div class="testimonials__button swiper-button-prev"><i class="fal fa-2x fa-long-arrow-left"></i></div>
                    <div class="testimonials__button swiper-button-next"><i class="fal fa-2x fa-long-arrow-right"></i></div>
                </div>

            </div>

        </div>
    </div>
</section>