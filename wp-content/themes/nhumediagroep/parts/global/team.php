<section class="team block">
    <?php ill_get_template_part('parts/global/section-header', 				
        [
        'title' => 'Samen zijn we een sterk team',
        'text' => 'Ons team bestaat dolor sit amet, consectetur adipiscing elit. Integer condimentum auctor odio id iaculis:',
        ]
    ); ?>    

    <div class="team__grid grid sm:grid-cols-12 container">
        <?php for ( $i = 0; $i < 8; $i++ ) : ?>
            <div data-aos="fade-up" class="team__member sm:col-span-6 md:col-span-4 lg:col-span-3">
                <div class="team__image aspect-ratio aspect-ratio--14/9">
                    <div class="aspect-ratio__inner">
                        <img src="<?php echo ill_asset_path( "images/portret.jpg" ); ?>" alt="image">
                    </div>
                </div>
                <div class="team__content">
                    <h3 class="team__heading">Harmen Weijer</h3>
                    <p>Hoofdredacteur RenovatieTotaal</p>
                    <?php get_template_part('parts/global/socials'); ?>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</section>