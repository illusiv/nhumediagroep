<?php

$social_facebook    = get_field('option_social_facebook', 'options');
$social_linkedin    = get_field('option_social_linkedin', 'options');
$option_email 		= esc_html( antispambot( get_field('option_email', 'options') ) );

?>

<ul class="socials xl:flex">

    <?php if ( $social_linkedin ) : ?>
    <li>
        <a href="<?php echo $social_linkedin; ?>" target="_blank">                    
            <i class="fab fa-sm fa-linkedin-in"></i>
        </a>
    </li>
    <?php endif; ?>

    <?php if ( $social_facebook ) : ?>
    <li>
        <a href="<?php echo $social_facebook; ?>" target="_blank">                    
            <i class="fab fa-sm fa-facebook-f"></i>
        </a>
    </li>
    <?php endif; ?>

    <?php if ( $option_email ) : ?>
    <li>
        <a href="mailto:<?php echo $option_email; ?>" target="_blank">                    
            <i class="fal fa-sm fa-envelope-open"></i>
        </a>
    </li>
    <?php endif; ?>

</ul>