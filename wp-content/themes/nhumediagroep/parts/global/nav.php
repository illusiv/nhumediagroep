<?php
    $option_address         = get_field('option_address', 'options');
    $option_email           = esc_html( antispambot( get_field('option_email', 'options') ) );
    $option_phone           = get_field('option_phone', 'options');
    $option_overlay_image   = wp_get_attachment_image( get_field('option_overlay_image', 'options'), 'large' );
?>
<div class="nav">
    <div class="nav__inner grid grid--no-gap lg:grid-cols-2 container">
        <div class="flex flex-col justify-between">

            <nav class="nav__navigation flex flex-col justify-center">

                <?php if ( has_nav_menu( 'primary-menu' ) ) : ?>

                    <?php wp_nav_menu( array(
                        'theme_location'	=> 'primary-menu',
                        'depth'				=> 1,
                        'container'			=> ''
                    )); ?>

                <?php endif; ?>

                <!-- <ul>
                    <li style="--animation-order: 1;"><a href="/titels">Onze titels</a></li>
                    <li style="--animation-order: 2;"><a href="/adverteren">Adverteren</a></li>
                    <li style="--animation-order: 3;"><a href="/cases">Cases</a></li>
                    <li style="--animation-order: 4;"><a href="/contentmarketing">Contentproductie</a></li>
                    <li style="--animation-order: 5;"><a href="/over-ons">Contact</a></li>
                </ul> -->

                <?php if ( has_nav_menu( 'secondary-menu' ) ) : ?>

                    <?php wp_nav_menu( array(
                        'theme_location'	=> 'secondary-menu',
                        'depth'				=> 1,
                        'container'			=> '',
                        'menu_class'        => 'flex'
                    )); ?>

                <?php endif; ?>

            </nav>

            <div class="nav__address">

                <address>

                    <?php echo $option_address; ?><br>

                    <?php if ( $option_phone ) : ?>
                        <a href="tel:<?php echo preg_replace( '/[^0-9]/', '', $option_phone ); ?>">
                            <span>t.</span> <?php echo $option_phone; ?>
                        </a>
                        <br />
                    <?php endif; ?>

                    <?php if ( $option_email ) : ?>
                        <a href="mailto:<?php echo $option_email; ?>">
                            <span>e.</span> <?php echo $option_email; ?>
                        </a>
                    <?php endif; ?>

                </address>

                <?php get_template_part('parts/global/social-icons'); ?>

            </div>

        </div>
        
        <?php if ( $option_overlay_image ) : ?>
        <div class="nav__image">
            <?php echo $option_overlay_image; ?>
        </div>
        <?php endif; ?>

    </div>
    
</div>