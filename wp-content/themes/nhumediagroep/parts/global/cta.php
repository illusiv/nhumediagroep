<aside data-aos="fade-up" class="cta lg:grid lg:grid--no-gap lg:grid-cols-12">

    <?php if ( $double ): ?>
    <div class="cta__content lg:col-span-6 bg-primary-dark block">
        <div class="container">
            <h2 class="h2 heading-underline heading-underline--center">Bekijk onze titels</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac orci vestibulum, efficitur massa non, semper lacus.</p>
            <a href="#" class="button button--primary-white button--arrow">Onze titels</a>
        </div>
    </div>
    <?php endif; ?>
    
    <div class="cta__content block <?= $double ? 'lg:col-span-6 bg-primary' : 'lg:col-span-full bg-primary-dark'?>">
        <div class="container">
            <h2 class="h2 heading-underline heading-underline--center <?= $double ? 'heading-underline--white' : ''?>">Hulp nodig bij content creatie?</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac orci vestibulum, efficitur massa non, semper lacus.</p>
            <a href="#" class="button button--arrow <?= $double ? 'button--primary-dark-white' : 'button--primary-white'?>">Meer informatie</a>
        </div>
    </div>
</aside>