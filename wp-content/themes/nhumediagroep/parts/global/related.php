<section class="related block">
    <?php ill_get_template_part('parts/global/section-header', 				
        [
            'title' => 'Andere advertentie mogelijkheden',
            'text' => 'Naast advertorials bieden wij dolor sit amet, consectetur adipiscing elit. Integer condimentum auctor odio id iaculis:'
        ]
    ); ?> 

    <div class="grid sm:grid-cols-12 container">
        <?php for ( $i = 0; $i < 4; $i++ ) : ?>
            <div class="related__item sm:col-span-6 lg:col-span-3">
                <div class="related__image aspect-ratio aspect-ratio--square">
                    <a href="#" class="aspect-ratio__inner">
                        <img src="<?php echo ill_asset_path( "images/case-1.jpg" ); ?>" alt="image">
                    </a>
                </div>
                <div class="related__content">
                    <a href="#">Product nieuws<i class="fal fa-long-arrow-right"></i></a>
                </div>
            </div>
        <?php endfor; ?>
    </div>

</section>