<?php

$cases_title    = get_sub_field('cases_title');
$cases_text     = get_sub_field('cases_text');
$cases_posts    = get_sub_field('case_posts');

$latest_cases = new WP_Query(array(
    'post_type' 		=> 'case',
    'posts_per_page' 	=> 3
));

?>

<div class="block">

    <div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

        <?php if ( $cases_title ) : ?>
        <div class="lg:col-start-4 lg:col-end-10">

            <h2 class="heading-underline heading-underline--center h2">
                <?php echo $cases_title; ?>
            </h2>

            <?php echo $cases_text; ?>

        </div>
        <?php endif; ?>

    </div>

    <?php if ( $latest_cases->have_posts() ) : ?>
    <div class="cases__grid">
        <div class="grid md:grid-cols-12 lg:grid-rows-2 container">

            <?php $counter = 0; while ( $latest_cases->have_posts() ) : $latest_cases->the_post(); $counter++; ?>

                <?php if ( $counter === 1 ) : ?>
                    <article data-aos="fade-up" class="cases__item cases__item--lg aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 md:aspect-ratio--21/9 lg:aspect-ratio--5/4 col-span-full lg:col-span-7 lg:row-span-2">
                <?php elseif ( $counter === 2 ) : ?>
                    <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 lg:aspect-ratio--auto col-span-full md:col-span-6 lg:col-span-5">
                <?php else : ?>
                    <article data-aos="fade-up" class="cases__item aspect-ratio aspect-ratio--4/3 xs:aspect-ratio--16/9 lg:aspect-ratio--auto col-span-full md:col-span-6 lg:col-span-5">
                <?php endif; ?>
                    <a href="<?php the_permalink(); ?>" class="cases__inner flex flex-row items-end aspect-ratio__inner">

                        <?php the_post_thumbnail('large'); ?>

                        <div class="cases__content">

                            <h3 class="cases__heading h5">
                                <?php the_title(); ?>
                            </h3>

                            <?php if ( get_field('case_overview_title') ) : ?>
                            <p class="h3">
                                <?php the_field('case_overview_title'); ?>
                            </p>
                            <?php endif; ?>

                        </div>

                    </a>
                </article>

            <?php endwhile; wp_reset_postdata(); ?>
            
        </div>
    </div>
    <?php endif; ?>

</div>