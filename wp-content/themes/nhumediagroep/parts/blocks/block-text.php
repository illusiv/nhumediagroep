<?php
    $content = get_sub_field('content');
?>
<div class="block">
    <div class="lg:grid lg:grid-cols-12 container">
        <div class="copy lg:col-start-2 lg:col-end-12">
            <?php echo $content; ?>
        </div>
    </div>
</div>