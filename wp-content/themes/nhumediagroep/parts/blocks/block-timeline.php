<?php

$title  = get_sub_field('title');
$items  = 'items';

?>
<section data-aos="fade-up" class="timeline container">

    <?php if ( $title ) : ?>
    <h2 class="timeline__heading">
        <?php echo $title; ?>
    </h2>
    <?php endif; ?>
    
    <?php if ( get_sub_field( $items ) ) : ?>
    <div class="timeline__inner">
        <ul class="timeline__list">

            <?php while ( has_sub_field( $items ) ) : ?>
                <div class="timeline__item">

                    <?php
                        $title		    = get_sub_field('title');
                        $text		    = get_sub_field('text');
                        $link		    = get_sub_field('link');
                        $image_id		= get_sub_field('icon');
                        $image_url		= wp_get_attachment_image_url( $image_id, 'large' );
                        $image_alt      = get_post_meta( $image_id, '_wp_attachment_image_alt', TRUE);
                        $image_title    = get_the_title( $image_id );
                    ?>

                    <article class="card" data-aos="fade-up">
                        <a href="<?php echo get_the_permalink($link); ?>" class="flex flex-col">

                            <?php if ( $image_id ) : ?>
                                <img class="lazyload card__icon" data-src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $image_title; ?>">
                            <?php endif; ?>

                            <?php if ( $title ) : ?>
                            <h3 class="card__heading h3">
                                <?php echo $title ?>
                            </h3>
                            <?php endif; ?>

                            <?php echo $text ?>
  
                            <i class="fal fa-lg fa-long-arrow-right"></i>

                        </a>
                    </article>

                </div>
            <?php endwhile; ?>

        </ul>
    </div>
    <?php endif; ?>

</section>