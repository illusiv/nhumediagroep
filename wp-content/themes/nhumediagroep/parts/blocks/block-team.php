<?php

$title      = get_sub_field('title');
$text       = get_sub_field('text');
$members    = 'members';

?>
<section class="team block">

    <div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

        <?php if ( $title ) : ?>
        <div class="lg:col-start-4 lg:col-end-10">

            <h2 class="heading-underline heading-underline--center h2">
                <?php echo $title; ?>
            </h2>

            <?php echo $text; ?>

        </div>
        <?php endif; ?>

    </div>

    <?php if ( get_sub_field( $members ) ) : ?>
    <div class="team__grid grid sm:grid-cols-12 container">
        <?php while ( has_sub_field( $members ) ) : ?>

            <?php
                $image      = wp_get_attachment_image( get_sub_field('image'), '556x860' );
                $name       = get_sub_field('name');
                $linkedin   = get_sub_field('linkedin');
                $facebook   = get_sub_field('facebook');
                $email      = get_sub_field('email');
                $subtitle   = get_sub_field('subtitle');
            ?>

            <div data-aos="fade-up" class="team__member sm:col-span-6 md:col-span-4 lg:col-span-3">

                <?php if ( $image ) : ?>
                <div class="team__image aspect-ratio aspect-ratio--14/9">
                    <div class="aspect-ratio__inner">
                        <?php echo $image; ?>
                    </div>
                </div>
                <?php endif; ?>

                <div class="team__content">

                    <?php if ( $name ) : ?>
                    <h3 class="team__heading"><?php echo $name; ?></h3>
                    <?php endif; ?>

                    <?php if ( $subtitle ) : ?>
                    <p><?php echo $subtitle; ?></p>
                    <?php endif; ?>

                    <ul class="socials md:flex">

                        <?php if ( $linkedin ) : ?>
                        <li>
                            <a href="<?php echo $linkedin; ?>" target="_blank">                    
                                <i class="fab fa-sm fa-linkedin-in"></i>
                            </a>
                        </li>
                        <?php endif; ?>

                        <?php if ( $facebook ) : ?>
                        <li>
                            <a href="<?php echo $facebook; ?>" target="_blank">                  
                                <i class="fab fa-sm fa-facebook-f"></i>
                            </a>
                        </li>
                        <?php endif; ?>

                        <?php if ( $email ) : ?>
                        <li>
                            <a href="mailto:<?php echo esc_html( antispambot( $email ) ); ?>" target="_blank">                    
                                <i class="fal fa-sm fa-envelope-open"></i>
                            </a>
                        </li>
                        <?php endif; ?>

                    </ul>

                </div>

            </div>

        <?php endwhile; ?>
    </div>
    <?php endif; ?>

</section>