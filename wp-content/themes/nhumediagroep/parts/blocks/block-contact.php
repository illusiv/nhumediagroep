<?php

$image      = wp_get_attachment_image_url( get_sub_field('image'), 'thumbnail' );
$image_alt  = get_post_meta( get_sub_field('image'), '_wp_attachment_image_alt', TRUE);
$title      = get_sub_field('title');
$text       = get_sub_field('text');
$phone      = get_sub_field('phone');
$email      = esc_html( antispambot( get_sub_field('email') ) );

?>
<div data-aos="fade-up" class="contact block">
    <div class="lg:grid lg:grid-cols-12 container">
        <div class="lg:col-start-2 lg:col-end-12 xl:col-start-3 xl:col-end-11">

            <aside class="contact__card">
                <div class="md:flex">

                    <?php if ( $image ) : ?>
                    <img class="avatar lazyload" data-src="<?php echo $image; ?>" width="170" height="170" alt="<?php echo $image_alt; ?>">
                    <?php endif; ?>

                    <div class="contact__info">

                        <?php if ( $title ) : ?>
                        <h2 class="h3">
                            <?php echo $title; ?>
                        </h2>
                        <?php endif; ?>

                        <?php if ( $title ) : ?>
                            <p><?php echo $text; ?></p>
                        <?php endif; ?>

                        <ul class="flex flex-wrap flex-col justify-center md:flex-row md:justify-start">

                            <?php if ( $phone ) : ?>
                            <li>
                                <a href="tel:<?php echo preg_replace( '/[^0-9]/', '', $phone ); ?>">
                                    <i class="fal fa-mobile"></i><?php echo $phone; ?>
                                </a>
                            </li>
                            <?php endif; ?>

                            <?php if ( $email ) : ?>
                            <li>
                                <a href="mailto:<?php echo $email; ?>">
                                    <i class="fal fa-envelope-open"></i><?php _e('stuur een mail','nhumediagroep'); ?>
                                </a>
                            </li>
                            <?php endif; ?>

                        </ul>

                    </div>

                </div>
            </aside>

        </div>
    </div>
</div>