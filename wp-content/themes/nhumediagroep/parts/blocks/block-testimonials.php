<?php

$title          = get_sub_field('title');
$text           = get_sub_field('text');
$items          = 'items';

?>
<div data-aos="fade-up" class="logos block">

    <div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

        <?php if ( $title ) : ?>
        <div class="lg:col-start-4 lg:col-end-10">

            <h2 class="heading-underline heading-underline--center h2">
                <?php echo $title; ?>
            </h2>

            <?php echo $text; ?>

        </div>
        <?php endif; ?>

    </div>

    <?php if ( get_sub_field( $items ) ) : ?>
    <div class="lg:grid lg:grid-cols-12 container">
        <div class="lg:col-start-2 lg:col-end-12 xl:col-start-3 xl:col-end-11">

            <div class="testimonials swiper-container">

                <div class="swiper-wrapper">

                    <?php while ( has_sub_field( $items ) ) : ?>

                        <?php
                            $image      = wp_get_attachment_image( get_sub_field('image'), '480x480' );
                            $title      = get_sub_field('title');
                            $subtitle   = get_sub_field('subtitle');
                            $text       = get_sub_field('text');
                        ?>

                        <div class="testimonials__item swiper-slide">

                            <?php if ( $image ) : ?>
                            <div class="testimonials__media">
                                <div class="aspect-ratio aspect-ratio--square">
                                    <div class="aspect-ratio__inner">
                                        <?php echo $image; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            
                            <?php if ( $text ) : ?>
                            <figure class="testimonials__content">

                                <blockquote>
                                    <?php echo $text; ?>
                                </blockquote>

                                <?php if ( $title ) : ?>
                                <figcaption>
                                   
                                    <?php echo $title; ?>

                                    <?php if ( $subtitle ) : ?>
                                    <cite><?php echo $subtitle; ?></cite>
                                    <?php endif; ?>

                                </figcaption>
                                <?php endif; ?>

                            </figure>
                            <?php endif; ?>

                        </div>

                    <?php endwhile; ?>

                </div>

                <?php if ( count ( get_sub_field( $items ) ) > 1 ) : ?>
                <div class="testimonials__buttons">
                    <div class="testimonials__button swiper-button-prev"><i class="fal fa-2x fa-long-arrow-left"></i></div>
                    <div class="testimonials__button swiper-button-next"><i class="fal fa-2x fa-long-arrow-right"></i></div>
                </div>
                <?php endif; ?>

            </div>

        </div>
    </div>
    <?php endif; ?>

</div>