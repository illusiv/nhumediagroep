<?php if ( get_sub_field( 'cta_blocks' ) ) : ?>
<aside data-aos="fade-up" class="cta lg:grid lg:grid--no-gap lg:grid-cols-12">

    <?php $count = count( get_sub_field( 'cta_blocks' ) ); ?>

    <?php while ( has_sub_field( 'cta_blocks' ) ) : ?>

        <?php
            $title          = get_sub_field('title');
            $text           = get_sub_field('text');
            $button         = get_sub_field('button');
            $bgcolor        = get_sub_field('bgcolor');

            if ( $bgcolor === 'primary-dark' ) {
                $button_style = 'primary-white';
            } else {
                $button_style = 'primary-dark-white';
            }
            
        ?>

        <?php 
            if ( $count > 1 ) {
                $col_class = 'lg:col-span-6';
            } else {
                $col_class = 'lg:col-span-full';
            }
        ?>

        <div class="cta__content block <?php echo $col_class; ?> bg-<?php echo $bgcolor; ?>">

            <div class="container">

                <?php if ( $title ) : ?>
                <h2 class="h2 heading-underline heading-underline--center">
                    <?php echo $title; ?>
                </h2>
                <?php endif; ?>

                <?php echo $text; ?>

                <?php if ( $button ) : ?>
                <a href="<?php echo $button['url']; ?>" class="button button--<?php echo $button_style; ?> button--arrow" target="<?php echo $button['target']; ?>">
                    <?php echo $button['title']; ?>
                </a>
                <?php endif; ?>

            </div>
        </div>

    <?php endwhile; ?>

</aside>
<?php endif; ?>