<?php

$image      = wp_get_attachment_image( get_sub_field('image'), 'full' );
$title      = get_sub_field('title');
$text       = get_sub_field('text');
$button     = get_sub_field('button');
$sublink    = get_sub_field('sublink');
$alignment  = get_sub_field('alignment');
$tags_show  = get_sub_field('tags_show');
$tags_title = get_sub_field('tags_title');
$anchor     = get_sub_field('anchor');
$tags_items = 'tags_items';

?>
<div <?php if ( $anchor ) echo 'id="' . $anchor . '"'; ?> data-aos="fade-up" class="block text-block<?php if ( $alignment === 'reversed' ) echo ' text-block--reversed'; ?>">
    <div class="md:grid md:grid-cols-12 md:items-center container">

        <?php if ( $image ) : ?>
        <div class="aspect-ratio aspect-ratio--16/9 md:aspect-ratio--square md:col-span-6 lg:col-start-2 lg:col-end-7">
            <a href="#" class="text-block__image aspect-ratio__inner">
                <?php echo $image; ?>
            </a>
        </div>
        <?php endif; ?>

        <div class="md:col-span-6 lg:col-start-7 lg:col-end-12">
            <div class="text-block__content">

                <?php if ( $title ) : ?>
                <h3 class="heading-underline h2">
                    <?php echo $title; ?>
                </h3>
                <?php endif; ?>

                <?php echo $text; ?>

                <?php if ( $tags_show ) : ?>
                <div class="tags">

                    <?php if ( $tags_title ) : ?>
                    <strong><?php echo $tags_title; ?></strong>
                    <?php endif; ?>

                    <?php if ( get_sub_field( $tags_items ) ) : ?>
                    <ul class="flex flex-wrap tags__list">
                        <?php while ( has_sub_field( $tags_items ) ) : ?>
                            <li class="tags__item">
                                <?php the_sub_field('text'); ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>

                </div>
                <?php endif; ?>

                <?php if ( $button || $sublink ): ?>
                    <ul class="inline-flex flex-wrap flex-col">

                        <?php if ( $button ) : ?>
                        <li>
                            <a href="<?php echo $button['url']; ?>" class="button button--primary button--arrow" target="<?php echo $button['target']; ?>">
                                <?php echo $button['title']; ?>
                            </a>
                        </li>
                        <?php endif; ?>

                        <?php if ( $sublink ) : ?>
                        <li>
                            <a href="<?php echo $sublink['url']; ?>" class="button button--link" target="<?php echo $sublink['target']; ?>">
                                <i class="fal fa-info-circle"></i><span><?php echo $sublink['title']; ?></span>
                            </a>
                        </li>
                        <?php endif; ?>

                    </ul>
                <?php endif; ?>

            </div>
        </div>

    </div>
</div>