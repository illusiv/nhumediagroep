<?php

$title  = get_sub_field('title');
$text   = get_sub_field('text');
$logos  = 'logos';

?>
<div data-aos="fade-up" class="logos block">

    <div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

        <?php if ( $title ) : ?>
        <div class="lg:col-start-4 lg:col-end-10">

            <h2 class="heading-underline heading-underline--center h2">
                <?php echo $title; ?>
            </h2>

            <?php echo $text; ?>

        </div>
        <?php endif; ?>

    </div>
    
    <?php if ( get_sub_field( $logos ) ) : ?>
    <ul class="grid items-center justify-items-center container">
        <?php while ( has_sub_field( $logos ) ) : ?>

            <?php
                $image  = wp_get_attachment_image( get_sub_field('image'), 'large' );
                $link   = get_sub_field('link');
            ?>

            <li>
                <?php if ( $link ) : ?>
                <a href="<?php echo $link; ?>" target="_blank" rel="noopener noreferrer">
                <?php endif; ?>

                    <?php echo $image; ?>

                <?php if ( $link ) : ?>
                </a>
                <?php endif; ?>
            </li>

        <?php endwhile; ?>
    </ul>
    <?php endif; ?>

</div>