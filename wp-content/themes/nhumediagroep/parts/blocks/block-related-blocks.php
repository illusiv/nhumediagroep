<?php

$title          = get_sub_field('title');
$text           = get_sub_field('text');
$blocks_items   = 'blocks_items';

?>
<div data-aos="fade-up" class="logos block">

    <div data-aos="fade-up" class="section__header lg:grid lg:grid-cols-12 container">

        <?php if ( $title ) : ?>
        <div class="lg:col-start-4 lg:col-end-10">

            <h2 class="heading-underline heading-underline--center h2">
                <?php echo $title; ?>
            </h2>

            <?php echo $text; ?>

        </div>
        <?php endif; ?>

    </div>

    <?php if ( get_sub_field( $blocks_items ) ) : ?>
    <div class="grid sm:grid-cols-12 container">
        <?php $counter = 0; while ( has_sub_field( $blocks_items ) ) : $counter++; ?>

            <div class="related__item sm:col-span-6 lg:col-span-3">

                <?php
                    $title		    = get_sub_field('title');
                    $link           = get_sub_field('link');
                    $image_id		= get_sub_field('image');
                    $image_url		= wp_get_attachment_image_url( $image_id, 'large' );
                    $image_alt      = get_post_meta( $image_id, '_wp_attachment_image_alt', TRUE);
                    $image_title    = get_the_title( $image_id );
                ?>

                <?php if ( $image_id ) : ?>
                <div class="related__image aspect-ratio aspect-ratio--square">
                    <a href="<?php echo $link['url']; ?>" class="aspect-ratio__inner">
                        <img class="lazyload" data-src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" title="<?php echo $image_title; ?>">
                    </a>
                </div>
                <?php endif; ?>

                <?php if ( $title ) : ?>
                <div class="related__content">
                    <a href="<?php echo $link['url']; ?>">
                        <?php echo $title; ?>
                        <i class="fal fa-long-arrow-right"></i>
                    </a>
                </div>
                <?php endif; ?>

            </div>

        <?php endwhile; ?>
    </div>
    <?php endif; ?>

</div>