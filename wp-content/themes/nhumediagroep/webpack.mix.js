let mix                 = require('laravel-mix');
let ImageminPlugin      = require('imagemin-webpack-plugin').default;
let CopyWebpackPlugin   = require('copy-webpack-plugin');
let imageminMozjpeg     = require('imagemin-mozjpeg');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.js('assets/scripts/main.js', 'scripts/')
   .sass('assets/styles/main.scss', 'styles/')
   .copyDirectory('assets/images', './dist/images')
   .copyDirectory('assets/fonts', './dist/fonts')
   .setPublicPath('dist')
   .options({
      processCssUrls: false,
      autoprefixer: {
        options: {
          browsers: [
            'last 6 versions',
          ]
        }
      },
   })
   .browserSync({
    notify: {
      styles: {
          top: 'auto',
          bottom: '0',
          right: 'auto',
          left: '0'
      }
    },
    proxy: 'nhumediagroep.test',
    files: [
      'dist/**/*',
      'assets/**/*'
    ]
   });

if ( mix.inProduction() ) {
    mix.version();
    //mix.version(['dist/images/**.*']);
    mix.webpackConfig({
    plugins: [
      // Copy the images folder and optimize all the images
      new CopyWebpackPlugin([{
        from: './assets/images',
        to: './images'
      }]),
      new ImageminPlugin({
        test: /\.(jpe?g|png|gif|svg)$/i,
        optipng: { optimizationLevel: 7 },
        gifsicle: { optimizationLevel: 3 },
        pngquant: { quality: '65-90', speed: 4 },
        svgo: { removeUnknownsAndDefaults: false, cleanupIDs: false },
        plugins: [imageminMozjpeg({ quality: 60 })],
     })
    ]
  })
}
