import 'lazysizes';
import AOS from 'aos'
// import 'lazysizes/plugins/unveilhooks/ls.unveilhooks';

import Swiper, {EffectFade, Autoplay, Navigation} from 'swiper';

(function($) {
	// SwiperCore.use([Navigation, Pagination]);
	
	$(document).on('ready', function() {

		// toggle overlay
		$( ".js-nav-toggle" ).on("click", function() {
			$('body').toggleClass( 'active-responsive-nav' );
		});

		// Init AOS library
		AOS.init({
			once: true,
		});
		
		// configure Swiper to use modules
		Swiper.use([EffectFade, Autoplay, Navigation]);
	
		const swiper = new Swiper('.swiper-container', {
			effect: 'fade',
			fadeEffect: { crossFade: true },
			autoplay: {
				delay: 5000,
			},
			loop: true,
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});
	});


})(jQuery);
