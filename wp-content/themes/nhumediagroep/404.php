<?php get_header(); ?>

<div>
	<div class="container">

        <h1><?php _e('404 - Pagina niet gevonden','nhumediagroep'); ?></h1>

        <p><?php _e('De pagina waar u naar op zoek bent bestaat niet meer, is mogelijk verplaatst of is tijdelijk niet beschikbaar.','nhumediagroep'); ?></p>

        <p><strong><?php _e('U kunt het volgende doen:','nhumediagroep'); ?></strong></p>

        <ul>
            <li>
            	<?php _e('Controleer uw spelling.','nhumediagroep'); ?>
            </li>
            <li>
            	<?php _e('Keer terug naar de','nhumediagroep'); ?> <a href="<?php echo home_url(); ?>"><?php _e('homepagina','nhumediagroep'); ?></a>.
            </li>
            <li>
            	<?php _e('Klik op de','nhumediagroep'); ?> <a href="javascript:history.back()"><?php _e('terug knop','nhumediagroep'); ?></a> <?php _e('van uw browser.','nhumediagroep'); ?>
            </li>
        </ul>

	</div>
</div>

<?php get_footer(); ?>
