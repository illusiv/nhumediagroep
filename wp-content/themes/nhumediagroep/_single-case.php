<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">

    <?php ill_get_template_part('parts/global/page-header', 				
        [
        'title' => 'Branded Content',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit massa et urna mollis, non fermentum velit semper. ',
        'image' => true,
        ]
    ); ?>


    <?php ill_get_template_part('parts/global/text-block',
            [
                'tags' => false,
                'button' => false,
                'link' => false
            ]
    ); ?> 
    
    <div class="lg:grid lg:grid-cols-12 container">
        <div class="copy lg:col-start-2 lg:col-end-12 xl:col-start-3 xl:col-end-11">
            <?php the_content(); ?>
        </div>
    </div>

    <?php get_template_part('parts/global/related'); ?> 

    <?php ill_get_template_part('parts/global/cta',
        [
        'double' => false
        ]
    ); ?>        

</main>

<?php endwhile; ?>

<?php get_footer(); ?>
