<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>

	<title><?php wp_title(); ?></title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="<?php bloginfo('charset'); ?>">

	<?php wp_head(); ?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>

	<?php the_field('option_head', 'options'); ?>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MF49DTF');</script>
	<!-- End Google Tag Manager -->

</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MF49DTF"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<header class="header">
		<div class="header__inner flex justify-between items-center container">

			<?php if ( ! is_front_page() ) echo '<a href="' . home_url() . '">'; ?>
				<svg class="logo <?php echo is_front_page() ? 'logo--homepage' : 'logo--lg'; ?>" xmlns="http://www.w3.org/2000/svg" fill="none" width="328" height="81" viewBox="0 0 328 81">
					<path class="logo__circle" fill-rule="evenodd" d="M164.13 4.2h5.202c.39 0 .651.26.651.39v33.805c0 .26-.261.52-.651.52h-5.202c-.39 0-.65-.26-.65-.52V23.313h-7.023v15.212c0 .26-.26.52-.651.52h-5.202c-.39 0-.65-.26-.65-.52V4.72c0-.26.26-.52.65-.52h5.202c.391 0 .651.26.651.52v13.651h7.023V4.721c0-.26.26-.52.65-.52zm-19.118 1h-5.202c-.39 0-.651.26-.651.52v17.293L133.957 5.46c0-.26-.26-.39-.65-.39h-5.202c-.39 0-.651.26-.651.52v33.804c0 .26.261.52.651.52h5.202c.39 0 .65-.26.65-.52V22.493l5.202 17.162v-.13c0 .26.261.52.651.52h5.202c.39 0 .65-.26.65-.52V5.72c-.13-.26-.39-.52-.65-.52zm48.12 0h-5.202c-.39 0-.65.26-.65.52v26.394c0 2.47-1.171 3.38-3.121 3.38-1.951 0-3.122-.91-3.122-3.38V5.72c0-.26-.26-.52-.65-.52h-5.202c-.39 0-.65.26-.65.52v26.003c0 5.59 3.251 8.711 9.494 8.711 6.242 0 9.494-3.12 9.494-8.711V5.72c.26-.26 0-.52-.391-.52zM205.098 50.446c-.651 0-1.301.65-1.301 1.3 0 .78.65 1.3 1.301 1.3.65 0 1.3-.52 1.3-1.3 0-.65-.65-1.3-1.3-1.3zm.91 6.761h-1.691V73.98h1.691V57.207zm-7.803-6.5v23.272h-1.691v-3.9c-1.431 2.6-3.902 4.29-7.153 4.29-4.812 0-8.714-3.77-8.714-8.71 0-4.942 3.902-8.712 8.714-8.712 3.251 0 5.852 1.69 7.153 4.29v-10.53h1.691zm-1.691 14.821c0-4.03-3.121-7.15-7.153-7.15s-7.153 3.12-7.153 7.15 3.121 7.151 7.153 7.151 7.153-3.12 7.153-7.15zm-41.228 8.451V63.318c0-4.16-2.601-6.501-6.112-6.501-2.211 0-4.422 1.04-5.593 3.38-.78-2.21-2.731-3.38-5.202-3.38-2.211 0-4.162.91-5.462 3.25v-2.86h-1.691V73.72h1.691v-9.361c0-4.29 2.471-6.11 5.202-6.11 2.601 0 4.292 1.82 4.292 4.94v10.661h1.691v-9.361c0-4.29 2.21-6.11 5.072-6.11 2.601 0 4.422 1.82 4.422 4.94v10.661h1.69zm21.459-7.54v-.78c0-4.682-3.121-8.842-8.193-8.712-5.202 0-8.714 3.77-8.714 8.711 0 5.07 3.642 8.711 8.844 8.711 3.251 0 5.853-1.43 7.283-3.77l-1.43-.91c-1.041 1.69-3.122 2.99-5.853 2.99-3.902 0-6.763-2.47-7.153-6.24h15.216zm-1.69-1.691h-13.526c.39-3.64 3.121-6.37 7.023-6.37 3.251 0 6.113 2.34 6.503 6.37zm53.583-7.54v16.771h-1.691v-3.77c-1.431 2.47-4.032 4.16-7.153 4.16-4.812 0-8.714-3.77-8.714-8.71 0-4.942 3.902-8.712 8.714-8.712 3.251 0 5.852 1.69 7.153 4.16v-3.9h1.691zm-1.691 8.32c0-4.03-3.121-7.15-7.023-7.15-3.902 0-7.153 3.12-7.153 7.15s3.121 7.151 7.153 7.151 7.023-3.12 7.023-7.15zm24.45 7.671V57.467h-1.69v3.9c-1.301-2.6-3.902-4.29-7.153-4.29-4.812 0-8.714 3.77-8.714 8.711 0 4.94 3.902 8.711 8.714 8.711 3.251 0 5.722-1.69 7.153-4.29v3.12c0 3.77-2.471 6.24-6.633 6.24-3.772 0-5.593-1.43-6.503-3.25l-1.431.78c1.171 2.47 4.032 4.031 7.934 4.031 4.552 0 8.323-2.86 8.323-7.931zm-8.843-14.822c4.031 0 7.153 3.12 7.153 7.151 0 4.03-3.122 7.151-7.153 7.151-4.032 0-7.153-3.12-7.153-7.15 0-4.031 3.121-7.152 7.153-7.152zm22.239-1.43v1.56c-2.861 0-5.592 1.82-5.592 6.111v9.361h-1.691V57.207h1.691v3.25c1.17-2.6 3.381-3.51 5.592-3.51zm11.185-.13c-4.942 0-8.844 3.77-8.844 8.711 0 4.94 3.902 8.841 8.844 8.711 4.942 0 8.844-3.77 8.844-8.71 0-4.941-4.032-8.712-8.844-8.712zm0 1.56c3.901 0 7.023 3.12 7.023 7.151 0 4.03-3.122 7.151-7.023 7.151-4.032 0-7.153-3.12-7.153-7.15 0-4.031 3.121-7.152 7.153-7.152zm29.522 7.281v.78h-15.216c.39 3.77 3.251 6.241 7.153 6.241 2.731 0 4.812-1.3 5.852-2.99l1.431.91c-1.431 2.34-4.032 3.77-7.283 3.77-5.202 0-8.844-3.64-8.844-8.71 0-4.942 3.512-8.712 8.714-8.712 5.072-.13 8.193 4.03 8.193 8.711zm-15.216-.91h13.526c-.391-4.03-3.252-6.37-6.503-6.37-3.902 0-6.633 2.73-7.023 6.37zm29.002 9.491c4.812 0 8.714-3.77 8.714-8.71 0-4.941-3.902-8.712-8.714-8.712-3.251 0-5.722 1.69-7.153 4.29v-3.9h-1.69V80.61h1.69V69.949c1.301 2.6 3.902 4.29 7.153 4.29zm-.13-15.862c4.032 0 7.153 3.12 7.153 7.151 0 4.03-3.121 7.151-7.153 7.151s-7.153-3.12-7.153-7.15c0-4.031 3.121-7.152 7.153-7.152z" clip-rule="evenodd"/>
					<path class="logo__circle" d="M103.134 77.23c-1.431 0-2.471-1.17-2.471-2.47V6.11c0-1.43 1.17-2.47 2.471-2.47 1.431 0 2.471 1.17 2.471 2.47v68.52c0 1.43-1.04 2.6-2.471 2.6zM145.012 5.2h-5.202c-.39 0-.651.26-.651.52v17.293L133.957 5.46c0-.26-.26-.39-.65-.39h-5.202c-.39 0-.651.26-.651.52v33.804c0 .26.261.52.651.52h5.202c.39 0 .65-.26.65-.52V22.493l5.202 17.162v-.13c0 .26.261.52.651.52h5.202c.39 0 .65-.26.65-.52V5.72c-.13-.26-.39-.52-.65-.52zM193.132 5.2h-5.202c-.39 0-.65.26-.65.52v26.394c0 2.47-1.171 3.38-3.121 3.38-1.951 0-3.122-.91-3.122-3.38V5.721c0-.26-.26-.52-.65-.52h-5.202c-.39 0-.65.26-.65.52v26.003c0 5.59 3.251 8.711 9.494 8.711 6.242 0 9.494-3.12 9.494-8.711V5.72c.26-.26 0-.52-.391-.52zM169.332 5.2h-5.202c-.39 0-.65.26-.65.52v13.652h-7.023V5.721c0-.26-.26-.52-.651-.52h-5.202c-.39 0-.65.26-.65.52v33.804c0 .26.26.52.65.52h5.202c.391 0 .651-.26.651-.52V24.313h7.023v15.082c0 .26.26.52.65.52h5.202c.39 0 .651-.26.651-.52V5.59c0-.13-.261-.39-.651-.39z"/>
					<path class="logo__circle" fill-rule="evenodd" d="M80.894 40.435c0 22.332-18.108 40.435-40.447 40.435C18.11 80.87 0 62.767 0 40.435S18.109 0 40.447 0c22.339 0 40.447 18.103 40.447 40.435zM28.352 29.904h2.991c.26 0 .39.13.52.26v19.632c0 .13-.13.26-.39.26h-2.99c-.261 0-.391-.13-.391-.26l-2.991-9.881v9.881c0 .13-.13.26-.39.26h-3.122c-.26 0-.39-.13-.39-.26V30.164c0-.13.13-.26.39-.26h2.992c.26 0 .39.13.39.26l2.99 10.141V30.164c0-.13.131-.26.391-.26zm27.962 0h2.991c.26 0 .39.13.39.26v15.082c0 3.25-1.95 5.07-5.592 5.07-3.642 0-5.592-1.82-5.592-5.07V30.164c0-.13.13-.26.39-.26h2.991c.26 0 .39.13.39.26V45.506c0 1.43.65 1.95 1.821 1.95 1.17 0 1.82-.52 1.82-1.95V30.164c0-.13.13-.26.391-.26zm-10.795 0h-2.99c-.261 0-.391.13-.391.26v7.93h-4.032v-7.93c0-.13-.13-.26-.39-.26h-2.991c-.26 0-.39.13-.39.26v19.632c0 .13.13.26.39.26h2.991c.26 0 .39-.13.39-.26v-8.84h4.032v8.84c0 .13.13.26.39.26h2.991c.26 0 .39-.13.39-.26V30.164c0-.13-.13-.26-.39-.26z" clip-rule="evenodd"/>
				</svg>
			<?php if ( ! is_front_page() ) echo '</a>'; ?>

			<?php get_template_part('parts/global/nav'); ?>

			<div class="flex items-center">

				<?php $option_phone = get_field('option_phone', 'options'); if ( $option_phone ) : ?>
				<a href="tel:<?php echo preg_replace( '/[^0-9]/', '', $option_phone ); ?>" class="header__phone">
					<i class="fal fa-mobile"></i><?php echo $option_phone; ?>
				</a>
				<?php endif; ?>

				<div class="header__nav-toggle">
					<button type="button" class="hamburger js-nav-toggle">
						<span></span>
					</button>
				</div>

			</div>

		</div>
	</header>
