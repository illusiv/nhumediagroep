<?php

/**
 * WordPress functions.php
 *
 * @file functions.php
 * @author Illusiv
 *
 */

include_once('includes/assets.php');
include_once('includes/branding.php');
include_once('includes/defaults.php');
include_once('includes/custom-post-types.php');
include_once('includes/shortcodes.php');
include_once('includes/acf.php');
include_once('includes/custom.php');
// include_once('includes/woocommerce.php');
include_once('includes/translations.php');
include_once('includes/no-index.php');