<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">

    <?php ill_get_template_part('parts/global/page-header', 				
        [
        'title' => 'Onze Titels',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit massa et urna mollis, non fermentum velit semper.',
        'image' => false,
        ]
    ); ?>

    <?php for ( $i = 0; $i < 2; $i++ ) : ?>
        <?php ill_get_template_part('parts/global/text-block',
            [
                'tags' => false,
                'button' => true,
                'link' => true,
            ]
        ); ?> 
    <?php endfor; ?>

    <?php ill_get_template_part('parts/global/cta',
        [
        'double' => false
        ]
    ); ?>    

    <?php for ( $i = 0; $i < 2; $i++ ) : ?>
        <?php ill_get_template_part('parts/global/text-block',
            [
                'tags' => false,
                'button' => true,
                'link' => true,
            ]
        ); ?> 
    <?php endfor; ?>

    <?php get_template_part('parts/global/logos'); ?>

    <?php get_template_part('parts/global/contact'); ?>

</main>

<?php endwhile; ?>

<?php get_footer(); ?>
