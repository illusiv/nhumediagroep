<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">

    <?php ill_get_template_part('parts/global/page-header', 				
        [
        'title' => 'Adverteren',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit massa et urna mollis, non fermentum velit semper.',
        'image' => false,
        ]
    ); ?>

    <?php for ( $i = 0; $i < 6; $i++ ) : ?>
        <?php ill_get_template_part('parts/global/text-block',
            [
                'tags' => true,
                'button' => true,
                'link' => false
            ]
        ); ?> 
    <?php endfor; ?>

    <?php ill_get_template_part('parts/global/cta',
        [
        'double' => false
        ]
    ); ?>    

    <?php get_template_part('parts/global/contact'); ?>

</main>

<?php endwhile; ?>

<?php get_footer(); ?>
