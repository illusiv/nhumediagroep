<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<main class="main">

    <?php ill_get_template_part('parts/global/page-header', 				
        [
        'title' => 'Advertorials',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit massa et urna mollis, non fermentum velit semper. ',
        'image' => false,
        ]
    ); ?>

    <?php for ( $i = 0; $i < 3; $i++ ) : ?>
        <?php ill_get_template_part('parts/global/text-block',
            [
                'tags' => false,
                'button' => false,
                'link' => false
            ]
        ); ?> 
    <?php endfor; ?>

    <?php ill_get_template_part('parts/global/cta',
        [
        'double' => true
        ]
    ); ?>     

    <?php get_template_part('parts/global/related'); ?>

    <?php get_template_part('parts/global/contact'); ?>

</main>

<?php endwhile; ?>

<?php get_footer(); ?>
