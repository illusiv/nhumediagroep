��    &      L  5   |      P     Q  ?   d  &   �  �   �  	   �     �     �     �     �  7   �  *        E     H     M     ^     m  M   s  �   �  S   F      �     �  
   �  
   �  
   �  /   �          .  	   3  
   =     H  	   Q  	   [     e     k     w  	   �  	   �  *  �     �  T   �  (   0	  �   Y	  	   %
     /
     @
     \
     m
  >   
  ,   �
     �
  
   �
     �
          #  F   )  �   p  a     "   n     �  
   �     �     �  /   �     �       
     
        )  	   .  	   8     B     J     V     b  	   n                                  	             &                                       %                                    !                    
          $               #   "        %s Active Installs Adds shortcodes to easily create columns in your posts or pages Are you happy with Columns Shortcodes? Be sure to check out other plugins by Codepress, such as %s. It adds custom columns to your posts, users, comments and media overview in your admin. Get more insight in your content now! Codepress Column Shortcodes Column padding ( optional ) Column shortcodes Download for Free Enter padding first, then select your column shortcode. I'm using Column Shortcodes for WordPress! No Rate Select shortcode Support Forums Tweet Use the input fields below to customize the padding of your column shortcode. We would really love it if you could show your appreciation by giving us a rating on WordPress.org or tweet about Column Shortcodes! What's wrong? Need help? Let us know: please open a support topic on WordPress.org! Woohoo! We're glad to hear that! Yes five sixth four fifth full width https://wordpress.org/plugins/column-shortcodes https://www.admincolumns.com/ last one fifth one fourth one half one sixth one third reset three fifth three fourth two fifth two third PO-Revision-Date: 2020-06-29 12:49:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Column Shortcodes - Stable (latest release)
 %s actieve installaties Gebruikt shortcodes om makkelijk kolommen in jouw berichten of paginas toe te voegen Bent je tevreden met Columns Shortcodes? Bekijk ook zeker andere plugins van Codepress, zoals %s. Het voegt aangepaste kolommen toe aan je berichten, gebruikers, opmerkingen en mediaoverzicht in je beheerder. Krijg nu meer inzicht in je inhoud! Codepress Kolom Shortcodes Kolom padding ( optioneel ) Kolom shortcodes Gratis downloaden Vul eerste de padding in, selecteer daarna je kolom shortcode. Ik gebruik Column Shortcodes voor WordPress! Nee Waardering Selecteer shortcode  Ondersteuningforums Tweet Vul onderstaande velden in om de padding van de columns aan te passen. We zouden het geweldig vinden als je je waardering zou kunnen tonen door ons een waardering te geven op WordPress.org of te tweeten over Column Shortcodes! Wat is er mis? Hulp nodig? Laat het ons weten: open een ondersteuningsonderwerp op WordPress.org! Woohoo! We zijn blij dat te horen! Ja vijf zesde vier vijfde volledige breedte https://wordpress.org/plugins/column-shortcodes https://www.admincolumns.com/ laatste een vijfde een vierde half een zesde een derde herstel drie vijfde drie vierde twee vijfde tweederde 